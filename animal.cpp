///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file animal.cpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author EmilyPham <emilyn3@hawaii.edu>
/// @brief  Lab 05a - Animal Farm 2 - EE 205 - Spr 2021
/// @date   17_FEB_2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <string>

#include "animal.hpp"

using namespace std;

namespace animalfarm {
	
void Animal::printInfo() {
	cout << "   Species = [" << species << "]" << endl;
	cout << "   Gender = [" << genderName( gender ) << "]" << endl;
}


string Animal::genderName (enum Gender gender) {
   switch (gender) {
      case MALE:    return string("Male");      break;
      case FEMALE:  return string("Female");    break;
      case UNKNOWN: return string("Unknown");   break;
   }

   return string("Really, really Unknown");
};
	

string Animal::colorName (enum Color color) {
/// Decode the enum Color into strings for printf()
   switch (color) {
      case BLACK:    return string("Black");       break;
      case WHITE:    return string("White");       break;
      case BROWN:    return string("Brown");       break;
      case SILVER:   return string("Silver");      break;
      case RED:      return string("Red");         break;
      case YELLOW:   return string("Yellow");      break;
      case GREEN:    return string("Green");       break;
      case BLUE:     return string("Blue");        break;
   }

   return string("Unknown");
};
	
} // namespace animalfarm
